URI-reference ::= [ absoluteURI | relativeURI ] [ "#" fragment ]
absoluteURI   ::= scheme ":" ( hier_part | opaque_part )
relativeURI   ::= ( net_path | abs_path | rel_path ) [ "?" query ]

hier_part     ::= ( net_path | abs_path ) [ "?" query ]
opaque_part   ::= uric_no_slash *uric

uric_no_slash ::= unreserved | escaped | ";" | "?" | ":" | "@" | "&" | "=" | "+" | "$" | ","

net_path      ::= "//" authority [ abs_path ]
abs_path      ::= "/"  path_segments
rel_path      ::= rel_segment [ abs_path ]

rel_segment   ::= 1*( unreserved | escaped | ";" | "@" | "&" | "=" | "+" | "$" | "," )

scheme        ::= alpha *( alpha | digit | "+" | "-" | "." )

authority     ::= reg_name | server

reg_name      ::= 1*( unreserved | escaped | "$" | "," | ";" | ":" | "@" | "&" | "=" | "+" )

server        ::= [ [ userinfo "@" ] hostport ]
userinfo      ::= 0*( unreserved | escaped | ";" | ":" | "&" | "=" | "+" | "$" | "," )

hostport      ::= host [ ":" port ]
host          ::= hostname | IPv4address
hostname      ::= 0*( domainlabel "." ) toplabel [ "." ]
domainlabel   ::= alphanum | alphanum *( alphanum | "-" ) alphanum
toplabel      ::= alpha | alpha *( alphanum | "-" ) alphanum
IPv4address   ::= 1*digit "." 1*digit "." 1*digit "." 1*digit
port          ::= 0*digit

path_segments ::= segment *( "/" segment )
segment       ::= 0*pchar *( ";" param )
param         ::= 0*pchar
pchar         ::= unreserved | escaped | ":" | "@" | "&" | "=" | "+" | "$" | ","

query         ::= 0*uric

fragment      ::= 0*uric

uric          ::= reserved | unreserved | escaped
reserved      ::= ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" | "$" | ","
unreserved    ::= alphanum | mark
mark          ::= "-" | "_" | "." | "!" | "~" | "*" | "'" | "(" | ")"

escaped       ::= "%" hex hex
hex           ::= digit | "A" | "B" | "C" | "D" | "E" | "F" | "a" | "b" | "c" | "d" | "e" | "f"

alphanum      ::= alpha | digit
alpha         ::= lowalpha | upalpha

lowalpha      ::= "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" | "m" |
                  "n" | "o" | "p" | "q" | "r" | "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z"
upalpha       ::= "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J" | "K" | "L" | "M" |
                  "N" | "O" | "P" | "Q" | "R" | "S" | "T" | "U" | "V" | "W" | "X" | "Y" | "Z"
digit         ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
