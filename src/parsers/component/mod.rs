/// `varchar 0*( [ "." ] varchar )`
pub fn varname(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize_pair(
        crate::parsers::char::varchar,
        nom::multi::many0_count(nom::sequence::pair(
            nom::combinator::opt(nom::bytes::complete::tag(".")),
            crate::parsers::char::varchar,
        )),
    )(i)
}

/// `"{" [ operator ] varname "}"`
pub fn expression(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize_tuple((
        nom::bytes::complete::tag("{"),
        nom::combinator::opt(crate::parsers::char::operator),
        crate::parsers::component::varname,
        nom::bytes::complete::tag("}"),
    ))(i)
}

/// `0*( literal | expression )`
pub fn uri_template(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize0(nom::branch::alt((
        crate::parsers::char::literal,
        crate::parsers::component::expression,
    )))(i)
}

#[cfg(test)]
mod tests;
