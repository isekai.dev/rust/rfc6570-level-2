/// `"%" hex hex`
pub fn escaped(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize_tuple((
        nom::bytes::complete::tag("%"),
        crate::parsers::char::hex,
        crate::parsers::char::hex,
    ))(i)
}

/// `":" | "/" | "?" | "#" | "[" | "]" | "@"`
pub fn gen_delim(i: &str) -> nom::IResult<&str, &str> {
    nom::combinator::recognize(nom::character::complete::one_of(":/?#[]@"))(i)
}

/// `digit | "A" | "B" | "C" | "D" | "E" | "F" | "a" | "b" | "c" | "d" | "e" | "f"`
pub fn hex(i: &str) -> nom::IResult<&str, &str> {
    nom::combinator::recognize(nom::character::complete::satisfy(|c| c.is_ascii_hexdigit()))(i)
}

/// `U+00E000 - U+00F8FF | U+0F0000 - U+0FFFFD | U+100000 - U+10FFFD`
pub fn i_private(i: &str) -> nom::IResult<&str, &str> {
    nom::combinator::recognize(nom::character::complete::satisfy(
        |c| matches!(c, '\u{00E000}'..='\u{00F8FF}' | '\u{0F0000}'..='\u{0FFFFD}' | '\u{100000}'..='\u{10FFFD}'),
    ))(i)
}

/// `unreserved | gen-delim | ucschar | iprivate | escaped | "!" | "$" | "&" | "(" | ")" | "*" | "+" | "," | ";" | "="`
pub fn literal(i: &str) -> nom::IResult<&str, &str> {
    nom::branch::alt((
        crate::parsers::char::unreserved,
        crate::parsers::char::gen_delim,
        crate::parsers::char::ucs_char,
        crate::parsers::char::i_private,
        crate::parsers::char::escaped,
        nom::combinator::recognize(nom::character::complete::one_of("!$&()*+,;=")),
    ))(i)
}

/// `"+" | "#"`
pub fn operator(i: &str) -> nom::IResult<&str, &str> {
    nom::combinator::recognize(nom::character::complete::one_of("+#"))(i)
}

/// ```txt
/// U+0000A0 - U+00D7FF | U+00F900 - U+00FDCF | U+00FDF0 - U+00FFEF |
/// U+010000 - U+01FFFD | U+020000 - U+02FFFD | U+030000 - U+03FFFD |
/// U+040000 - U+04FFFD | U+050000 - U+05FFFD | U+060000 - U+06FFFD |
/// U+070000 - U+07FFFD | U+080000 - U+08FFFD | U+090000 - U+09FFFD |
/// U+0A0000 - U+0AFFFD | U+0B0000 - U+0BFFFD | U+0C0000 - U+0CFFFD |
/// U+0D0000 - U+0DFFFD | U+0E1000 - U+0EFFFD
/// ```
pub fn ucs_char(i: &str) -> nom::IResult<&str, &str> {
    nom::combinator::recognize(nom::character::complete::satisfy(|c| {
        matches!(c,
            '\u{0000A0}'..='\u{00D7FF}' | '\u{00F900}'..='\u{00FDCF}' | '\u{00FDF0}'..='\u{00FFEF}' |
            '\u{010000}'..='\u{01FFFD}' | '\u{020000}'..='\u{02FFFD}' | '\u{030000}'..='\u{03FFFD}' |
            '\u{040000}'..='\u{04FFFD}' | '\u{050000}'..='\u{05FFFD}' | '\u{060000}'..='\u{06FFFD}' |
            '\u{070000}'..='\u{07FFFD}' | '\u{080000}'..='\u{08FFFD}' | '\u{090000}'..='\u{09FFFD}' |
            '\u{0A0000}'..='\u{0AFFFD}' | '\u{0B0000}'..='\u{0BFFFD}' | '\u{0C0000}'..='\u{0CFFFD}' |
            '\u{0D0000}'..='\u{0DFFFD}' | '\u{0E1000}'..='\u{0EFFFD}'
        )
    }))(i)
}

/// `alphanum | "-" | "." | "_" | "~"`
pub fn unreserved(i: &str) -> nom::IResult<&str, &str> {
    nom::combinator::recognize(nom::branch::alt((
        nom::character::complete::satisfy(|c| c.is_ascii_alphanumeric()),
        nom::character::complete::one_of("-._~"),
    )))(i)
}

/// `alphanum | escaped | "_"`
pub fn varchar(i: &str) -> nom::IResult<&str, &str> {
    nom::branch::alt((
        nom::combinator::recognize(nom::character::complete::satisfy(|c| c.is_ascii_alphanumeric())),
        crate::parsers::char::escaped,
        nom::bytes::complete::tag("_"),
    ))(i)
}

#[cfg(test)]
mod tests;
