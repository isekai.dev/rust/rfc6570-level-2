use anyhow::Context;

test_cases! {
    char::operator: {
        case_hash: "#",
        case_plus: "+"
    }
}
