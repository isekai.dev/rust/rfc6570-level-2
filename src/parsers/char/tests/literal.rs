use anyhow::Context;

test_cases! {
    char::literal: {
        case_ampersand: "&",
        case_asterisk: "*",
        case_comma: ",",
        case_dollar: "$",
        case_equals: "=",
        case_exclamation: "!",
        case_parenthesis_left: "(",
        case_parenthesis_right: ")",
        plus: "+",
        semicolon: ";"
    }
}
