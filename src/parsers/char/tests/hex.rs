use anyhow::Context;

test_cases! {
    char::hex: {
        case_0: "0",
        case_1: "1",
        case_2: "2",
        case_3: "3",
        case_4: "4",
        case_5: "5",
        case_6: "6",
        case_7: "7",
        case_8: "8",
        case_9: "9",
        case_a: "a",
        case_a_upper: "A",
        case_b: "b",
        case_b_upper: "B",
        case_c: "c",
        case_c_upper: "C",
        case_d: "d",
        case_d_upper: "D",
        case_e: "e",
        case_e_upper: "E",
        case_f: "f",
        case_f_upper: "F"
    }
}
